const Usuario = require('../../models/usuario');

exports.usuario_list = async function (req, res) {
    const users = await Usuario.allUsers();
    res.status(200).json(users);
}

exports.usuario_create_post = async function (req, res) {
    const { nombre } = req.body;
    const newUser = await Usuario.add({ nombre });
    res.status(201).json(newUser);
}

exports.usuario_delete = async function (req, res) {
    await Usuario.removeById(req.params.id);
    res.status(204).send();
}

exports.usuario_update_post = async function (req, res) {
    await Usuario.updateUser(req.params.id, req.body);
    res.status(200).json({ usuarios: await Usuario.allUsers() });
}

exports.usuario_reservar = async function (req, res) {
    Usuario.findUserById(req.body.id, (err, usuario) => {
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err) {
            res.status(200).send();
        });
    });
}