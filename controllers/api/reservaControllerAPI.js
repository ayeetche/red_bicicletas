const Reserva = require('../../models/reserva');

exports.reserva_list = async function (req, res) {
    const reservations = await Reserva.allReservations();
    res.status(200).json(reservations);
}