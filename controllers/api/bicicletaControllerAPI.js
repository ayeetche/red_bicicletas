const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = async function(req,res) {
    const bicis = await Bicicleta.allBicis();
    res.status(200).json(bicis);
}

exports.bicicleta_create_post = async function(req, res) {
    const { code, color, modelo, lat, lng } = req.body;
    const newBici = await Bicicleta.add({code, color, modelo, ubicacion: [lat, lng]});
    res.status(201).json(newBici);
}

exports.bicicleta_delete = async function(req,res) {
    await Bicicleta.removeByCode(req.params.id);
    res.status(204).send();
}

exports.bicicleta_update_post = async function(req,res) {
    await Bicicleta.updateBici(req.params.id, req.body);
    res.status(200).json({bicis: await Bicicleta.allBicis()});
}