const Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req,res) {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res) {
    const {code, color, modelo, lat, lng} = req.body;
    const bici = new Bicicleta(code, color, modelo, [lat, lng]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(parseInt(req.body.id, 10));
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function(req,res) {
    const id = parseInt(req.params.id, 10);
    const bici = Bicicleta.allBicis.find(bici => bici.id === id);
    res.render('bicicletas/update', { bici });
}

exports.bicicleta_update_post = function(req,res) {
    Bicicleta.update(req.body);
    res.redirect('/bicicletas');
}