**Red bicicletas**

Proyecto de NodeJs junto con [Express](https://expressjs.com/es/) y plantillas en [Pug JS](https://pugjs.org/api/getting-started.html).
Consiste en una plataforma que muestra un listado de bicicletas dentro de un mapa (C.A.B.A, Argentina) con la posibilidad del ingresar nuevas bicicletas, editarlas o eliminarlas.

---

## Para levantar el proyecto de forma local

Requisito tener instalado [NodeJs](https://nodejs.org/es/).
1. Clonar el repositorio desde la terminal `git clone https://ayeetche@bitbucket.org/ayeetche/red_bicicletas.git` o descargarlo.
2. Desde la terminal entrar a la ubicación donde se encuentra el repositorio e ingresar `npm install` para descargar los módulos.
3. Ejecutar `npm run devstart` para iniciar la api.
4. Ingresar a `http://localhost:5000/` desde un navegador web.

Home con mapa de bicicletas `http://localhost:5000/`
Listado de bicicletas `http://localhost:5000/bicicletas/`, donde el usuario tiene la posibilidad de ingresar una nueva bicicleta o editar/eliminar alguna de las que ya existen.

## API

Se desarrollaron endpoints para obtener el listado de bicicletas, crear una nueva o eliminarla.
Para probarlo es necesario tener instalado [Postman](https://www.postman.com/downloads/).

Endpoints:
- **GET localhost:5000/api/bicicletas/** Para ver un array con las bicicletas existentes.
- **POST localhost:5000/api/bicicletas/** Para ingresar una nueva bicicleta.
- **DELETE localhost:5000/api/bicicletas/:id/delete** Para borrar una bicicleta existente (reemplazar :id por el número que desea borrar).