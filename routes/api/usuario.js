const express = require('express');
const router = express.Router();

const usuarioController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioController.usuario_list);
router.post('/create', usuarioController.usuario_create_post);
router.post('/reservar', usuarioController.usuario_reservar);
router.post('/:id/update', usuarioController.usuario_update_post);
router.delete('/:id/delete', usuarioController.usuario_delete);

module.exports = router;
