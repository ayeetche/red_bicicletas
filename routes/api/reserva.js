const express = require('express');
const router = express.Router();

const reservaController = require('../../controllers/api/reservaControllerAPI');

router.get('/', reservaController.reserva_list);

module.exports = router;
