const mongoose = require('mongoose');
const Reserva = require('./reserva');

const usuarioSchema = new mongoose.Schema({
    nombre: String,
});

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
    const reserva = new Reserva({ 
        usuario: this._id,
        bicicleta: biciId,
        desde,
        hasta,
    });
    reserva.save(cb);
};

usuarioSchema.statics.allUsers = async function (cb) {
    return await this.find({}, cb);
};

usuarioSchema.statics.add = async function (u, cb) {
    return await this.create(u, cb);
};

usuarioSchema.statics.findUserById = function (_id, cb) {
    return this.findById(_id, cb);
};

usuarioSchema.statics.removeById = function (_id, cb) {
    return this.deleteOne({ _id }, cb);
};

usuarioSchema.statics.updateUser = function (_id, body, cb) {
    return this.update({_id}, body, cb);
};


module.exports = mongoose.model('Usuario', usuarioSchema);
