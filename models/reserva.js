const mongoose = require('mongoose');
const moment = require('moment');

const reservaSchema = new mongoose.Schema({
    desde: Date,
    hasta: Date,
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'},
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'},
});

reservaSchema.methods.diasDeReserva = function() {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
};

reservaSchema.statics.allReservations = async function (cb) {
    return await this.find({}, cb);
};

module.exports = mongoose.model('Reserva', reservaSchema);