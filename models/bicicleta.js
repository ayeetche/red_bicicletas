const mongoose = require('mongoose');

const bicicletaSchema = new mongoose.Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true,
        }
    },
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code, color, modelo, ubicacion
    })
}

bicicletaSchema.methods.toString = function () {
    return 'code: ' + this.code + " | color: " + this.color;
}

bicicletaSchema.statics.allBicis = async function (cb) {
    return await this.find({}, cb);
}

bicicletaSchema.statics.add = async function (aBici, cb) {
    return await this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function (code, cb) {
    return this.findOne({ code }, cb);
};

bicicletaSchema.statics.removeByCode = function (code, cb) {
    return this.deleteOne({ code }, cb);
};

bicicletaSchema.statics.updateBici = function (code, body, cb) {
    return this.update({code}, body, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);