const mymap = L.map('mainmap').setView([-34.62733, -58.43399], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYXllZXRjaGUiLCJhIjoiY2tpbzQ2M3diMHB1ZzJ3bzhic3A5ejF3NyJ9.myHWd_W_LkDVeZzYtWtC5A'
}).addTo(mymap);

$(document).ready( () => {
    const url = '/api/bicicletas/';
    $.ajax({
        url: url,
        type: 'GET',
        success: (data) => {
            data.bicis.forEach(bici => {
                L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
            });
        },
        error: (err) => console.error(`Error: ${JSON.stringify(err)}`)
    })
});