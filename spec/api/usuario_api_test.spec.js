const mongoose = require('mongoose');
const Usuario = require('../../models/usuario');
const request = require('request');

describe('Usuario API' , () => {
    beforeEach((done) => {
        mongoose.connect('mongodb://localhost/red_bicicletas', { useNewUrlParser: true });
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function () {});
        done();
    });
    afterEach((done) => {
        Usuario.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            done();
        });
    });

    describe('GET Usuarios /' , () => {
        it('Status 200' , (done) => {
            const nuevoUsuario = {nombre: "Raul"};
            Usuario.add(nuevoUsuario, (err, usuario) => {
                if (err) console.log(err);
                request.get('http://localhost:5000/api/usuarios', function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    done();
                });
            });
        });
    });

    describe('POST Usuarios /create' , () => {
        it('Status 201' , (done) => {
            const headers = {'content-type': 'application/json',};
            const usuario1 = '{ "nombre": "Carlos" }';

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/usuarios/create',
                body: usuario1
            }, function(error, response, body){
                Usuario.allUsers((err, usuarios) => {
                    expect(response.statusCode).toBe(201);
                    expect(usuarios.length).toBe(1);
                    done();
                })
            });
        });
    });

    describe('DELETE Usuarios /:id/delete' , () => {
        it('Status 204' , (done) => {
            const usuario = {
                _id: new mongoose.Types.ObjectId("5ffa440b4358d994ed2dfee4"), 
                nombre: "Raul"
            };
            Usuario.add(usuario, (err, bici) => {
                const headers = {'content-type': 'application/json',};
                request.delete({
                    headers: headers,
                    url: 'http://localhost:5000/api/usuarios/5ffa440b4358d994ed2dfee4/delete',
                }, function(error, response, body){
                    Usuario.allUsers((err, usuarios) => {
                        expect(response.statusCode).toBe(204);
                        expect(usuarios.length).toBe(0);
                        done();
                    })
                });
            });

        });
    });
    describe('UPDATE Usuarios /:id/update' , () => {
        it('Status 200' , (done) => {
            const usuario1 = {
                _id: new mongoose.Types.ObjectId("5ffa440b4358d994ed2dfee4"), 
                nombre: "Raul"
            };
            Usuario.add(usuario1, (err, u) => {
                const headers = {'content-type': 'application/json',};
                const usuario2 = '{ "nombre": "Carlos" }';

                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/usuarios/5ffa440b4358d994ed2dfee4/update',
                    body: usuario2
                }, function(error, response, body){
                    Usuario.findUserById(usuario1._id, (err, usuario) => {
                        expect(response.statusCode).toBe(200);
                        expect(usuario.nombre).toBe('Carlos');
                        done();
                    })
                });
            });
        });
    });
});