const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

describe('Bicicleta API' , () => {
    describe('GET Bicicletas /' , () => {
        it('Status 200' , (done) => {
            const newBici = {code: 1, color: 'rojo', modelo: 'urbana', ubicacion: [-34.604618768008834, -58.40433538190959]}
            Bicicleta.add(newBici, (err, bici) => {
                if (err) console.log(err);
                request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    done();
                });
            });
        });
    });

    describe('POST Bicicletas /create' , () => {
        it('Status 201' , (done) => {
            const headers = {'content-type': 'application/json',};
            const bici1 = '{ "id": 1, "color": "rojo", "modelo": "urbana", "lat": -34.604618768008834, "lng": -58.40433538190959}';

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: bici1
            }, function(error, response, body){
                Bicicleta.allBicis((err, bicis) => {
                    expect(response.statusCode).toBe(201);
                    expect(bicis.length).toBe(1);
                    done();
                })
            });
        });
    });

    describe('DELETE Bicicletas /:id/delete' , () => {
        it('Status 204' , (done) => {
            const bici1 = {code: 1, color: 'rojo', modelo: 'urbana', ubicacion: [-34.604618768008834, -58.40433538190959]}
            Bicicleta.add(bici1, (err, bici) => {
                const headers = {'content-type': 'application/json',};
                request.delete({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/1/delete',
                }, function(error, response, body){
                    Bicicleta.allBicis((err, bicis) => {
                        expect(response.statusCode).toBe(204);
                        expect(bicis.length).toBe(0);
                        done();
                    })
                });
            });

        });
    });
    describe('UPDATE Bicicletas /:id/update' , () => {
        it('Status 200' , (done) => {
            const bici1 = {code: 1, color: 'rojo', modelo: 'urbana', ubicacion: [-34.604618768008834, -58.40433538190959]}
            Bicicleta.add(bici1, (err, bici) => {
                const headers = {'content-type': 'application/json',};
                const newBici = '{ "id": 123, "color": "verde", "modelo": "montaña", "lat": -38.604618768008834, "lng": -54.40433538190959}';
    
                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/1/update',
                    body: newBici
                }, function(error, response, body){
                    Bicicleta.findByCode(bici1.code, (err, bici) => {
                        expect(response.statusCode).toBe(200);
                        expect(bici.color).toBe('verde');
                        expect(bici.modelo).toBe('montaña');
                        done();
                    })
                });
            });

        });
    });
});