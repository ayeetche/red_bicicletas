const mongoose = require('mongoose');
const Reserva = require('../../models/reserva');
const Usuario = require('../../models/usuario');
const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

describe('Reserva API' , () => {
    describe('GET Reservas /' , () => {
        it('Status 200' , (done) => {
            const usuario = new Usuario({
                nombre: "Raul"
            });
            usuario.save();
            const bicicleta = new Bicicleta({
                _id: new mongoose.Types.ObjectId("5ffa469fdb54a49abd310694"),
                code: 1, 
                color: 'rojo', 
                modelo: 'urbana', 
                ubicacion: [-34.604618768008834, -58.40433538190959]
            });
            bicicleta.save();

            const desde = new Date("2021-01-01");
            const hasta = new Date("2021-01-10"); 
            
            usuario.reservar(bicicleta._id, desde, hasta, (err, r) => {
                if (err) console.log(err);
                Reserva.allReservations((err, reservas) => {
                    request.get('http://localhost:5000/api/reservas', function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        done();
                    });
                });
            });
        });
    });
});