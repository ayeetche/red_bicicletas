const mongoose = require('mongoose');
const Usuario = require('../../models/usuario');
const Bicicleta = require('../../models/bicicleta');
const Reserva = require('../../models/reserva');

describe('Usuario', () => {

    beforeEach((done) => {
        mongoose.connect('mongodb://localhost/red_bicicletas', { useNewUrlParser: true });
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function () {});
        done();
    });
    afterEach((done) => {
        Usuario.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            Reserva.deleteMany({}, function(err, success) {
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success) {
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });
    
    describe('Usuario.allUsers', () => {
        it('comienza vacia', (done) => {
            Usuario.allUsers( (err, usuarios) => {
                expect(usuarios.length).toBe(0);
                done();
            });
        });
    });
    
    describe('Usuario.add', () => {
        it('agregamos uno', (done) => {
            const usuario = {nombre: "Raul"}
            Usuario.add(usuario, (err, u) => {
                if (err) console.log(err);
                Usuario.allUsers( (err, usuarios) => {
                    expect(usuarios.length).toBe(1);
                    expect(usuarios[0].nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
    
    describe('Usuario.findById', () => {
        it('devuelve usuario por id', (done) => {
            const usuario1 = {
                _id: new mongoose.Types.ObjectId("5ffa469fdb54a49abd310694"),
                nombre: "Raul"
            };
            const usuario2 = {
                _id: new mongoose.Types.ObjectId("6ffa469fdb54a49abd310694"),
                nombre: "Carlos"
            };
            Usuario.add(usuario1, (err, u1) => {
                if (err) console.log(err);
                Usuario.add(usuario2, (err, u2) => {
                    if(err) console.log(err);
                    Usuario.findById(usuario2._id, (err, usuario) => {
                        expect(usuario._id).toEqual(usuario2._id);
                        done();
                    });
                })
            });
        });
    });
    
    describe('Usuario.removeById', () => {
        it('elimina usuario por id', (done) => {
            const usuario = {
                _id: new mongoose.Types.ObjectId("5ffa469fdb54a49abd310694"),
                nombre: "Raul"
            };
            Usuario.add(usuario, (err, usuario1) => {
                if (err) console.log(err);
                Usuario.removeById(usuario1._id, (err, result) => {
                    if (err) console.log(err);
                    Usuario.allUsers( (err, usuarios) => {
                        expect(usuarios.length).toBe(0);
                        done();
                    });
                });
            });
        });
    });
    
    describe('Usuario.reservar', () => {
        it('debe existir una reserva', (done) => {
            const usuario = new Usuario({
                _id: new mongoose.Types.ObjectId("5ffa469fdb54a49abd310694"),
                nombre: "Raul"
            });
            const bicicleta = new Bicicleta({
                _id: new mongoose.Types.ObjectId("9ffa469fdb54a49abd310694"),
                code: 1, 
                color: 'rojo', 
                modelo: 'urbana', 
                ubicacion: [-34.604618768008834, -58.40433538190959]
            });
            usuario.save();
            bicicleta.save();
            const hoy = new Date();
            const mañana = new Date();
            mañana.setDate(hoy.getDate() +1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva) {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});
