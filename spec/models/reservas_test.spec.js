const mongoose = require('mongoose');
const Reserva = require('../../models/reserva');

beforeEach((done) => {
    mongoose.connect('mongodb://localhost/red_bicicletas', { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {});
    done();
});
afterEach((done) => {
    Reserva.deleteMany({}, function(err, success) {
        if(err) console.log(err);
        done();
    });
});

describe('Reserva.allReservations', () => {
    it('comienza vacia', (done) => {
        Reserva.allReservations( (err, reservas) => {
            expect(reservas.length).toBe(0);
            done();
        });
    });
});
