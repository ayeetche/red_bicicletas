const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

beforeEach((done) => {
    mongoose.connect('mongodb://localhost/red_bicicletas', { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {});
    done();
});
afterEach((done) => {
    Bicicleta.deleteMany({}, function(err, success) {
        if(err) console.log(err);
        done();
    });
});

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', (done) => {
        Bicicleta.allBicis( (err, bicis) => {
            expect(bicis.length).toBe(0);
            done();
        });
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', (done) => {
        const bici = {code: 1, color: 'rojo', modelo: 'urbana', ubicacion: [-34.604618768008834, -58.40433538190959]}
        Bicicleta.add(bici, (err, newBici) => {
            if (err) console.log(err);
            Bicicleta.allBicis( (err, bicis) => {
                expect(bicis.length).toBe(1);
                expect(bicis[0].code).toBe(bici.code);
                expect(bicis[0].color).toBe(bici.color);
                done();
            });
        });
    });
});

describe('Bicicleta.findByCode', () => {
    it('devuelve bici con code: 2', (done) => {
        const bici1 = {code: 1, color: 'rojo', modelo: 'urbana', ubicacion: [-34.604618768008834, -58.40433538190959]};
        const bici2 = {code: 2, color: 'verde', modelo: 'montaña', ubicacion: [-33.604618768008834, -54.40433538190959]};
        Bicicleta.add(bici1, (err, bici) => {
            if (err) console.log(err);
            Bicicleta.add(bici2, (err, bici) => {
                if (err) console.log(err);
                Bicicleta.findByCode(bici2.code, function(err, bici) {
                    expect(bici.code).toBe(bici2.code);
                    expect(bici.color).toBe(bici2.color);
                    expect(bici.modelo).toBe(bici2.modelo);
                    done();
                });
            });
        });
    });
});

describe('Bicicleta.removeByCode', () => {
    it('elimina bici con id 2', (done) => {
        const bici = {code: 2, color: 'blanca', modelo: 'urbana', ubicacion: [-34.59640122192812, -58.41801345405964]};

        Bicicleta.add(bici, (err, biciToRemove) => {
            if (err) console.log(err);
            Bicicleta.removeByCode(biciToRemove.code, (err, result) => {
                if (err) console.log(err);
                Bicicleta.allBicis( (err, bicis) => {
                    expect(bicis.length).toBe(0);
                    done();
                });
            });
        });
    });
});
